Patricia Amdfiefijeig Warmansonwitzescuskymansteinsenberg
qtcasen_warmansonwitzescuskymansteinsenberg_1353138458@tfbnw.net
z612o3z774g
100004695690597
private friends
friends: mike, susan

Susan Amdfedcfdbff Chaisonskyescusteinsenmanbergwitz
tcbqouh_chaisonskyescusteinsenmanbergwitz_1353138536@tfbnw.net
9y4pyhdzh0s
100004654364266
public friends
friends: patricia

Mike Amdfhaaecfjf Yangwitz
zshbggl_yangwitz_1353139420@tfbnw.net
efsichkcqq8
100004681153606
public friends
friends: patricia

Karen Amdfhfigcfaj Yangsen
kkvdsqb_yangsen_1353138509@tfbnw.net
cg24zxisq7k
100004686973610
private friends
friends: none

Richard Amdgciigkcb Greenestein
cmmimae_greenestein_1353139887@tfbnw.net
li4uwpmufjk
100004739970032
public friends
friends: none

1. Find all blue friends of a red
2. Determine if two reds are friends
3. Find all red friends of a red

P. Passive attack
A. Active attack

1 P. Success
Limitation: Groups of connected mutual friends, possible to jump into nets of friends and explore the current net
1 A. If you become friends with the target and are already friends with many users, you automatically check for common connections, potentially revealing new friend nets

2 P. If there is more than one arrangement of the edges while keeping the degrees of the vertices the same a passive attack is possible. If the property is not satisifed, you can still get a percentage chance that two are friends based on the relative degree.
2 A. Become friends with one target. Record the number of mutual connections. Become friends with the other. Comapre the number of mutual connections.

3. P.
The more blue friends a red has, the more graphs of reds it's a member of. This means that the blues can disclose information about the reds.
Limitation: We need to find all blue friends of a red to begin to find the red friends of a red.
Limitation: Two reds with no mutual adjacent blue can't be attacked passively