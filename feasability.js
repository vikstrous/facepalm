function is_connected(x,y,state){
  //y is smaller or equal to x
  return !! (state & (1 << (x*(x-1)/2 + y)));
}

function degree(node, size, state){
  var deg = 0;
  for(var x = 0; x < size; x++){
    if(node < x && is_connected(x,node,state))
      deg++;
    else if(node > x && is_connected(node,x,state))
      deg++;
  }
  return deg;
}

for ( var size = 2; size <= 4; size++){
  console.log("running for size:" + size);
  var connections = [];
  for ( var i = 0; i < size; i++) {
    var row = [];
    row.length = size;
    connections.push(row);
  }
  
  collisions = {};
  
  for (var state = 0; state < Math.pow(2, size*(size-1)/2); state++){
    var degrees = '';
    for(var node = 0; node < size; node++){
      degrees += degree(node, size, state) + ';';
    }
  
    collisions[degrees] = collisions[degrees]?collisions[degrees]+1:1;
  }
  
  var ambiguous = 0;
  var unambiguous = 0;
  for(var i in collisions){
    if(collisions[i] > 1){
      ambiguous += collisions[i];
    } else {
      unambiguous += 1;
    }
  }
  console.log(ambiguous, 'ambiguous');
  console.log(unambiguous, 'unambiguous');
  
  //visualize:
  /*
  for (var state = 0; state < Math.pow(2, size*(size-1)/2); state++){
    for (var x = 0; x < size; x++){
      for (var y = 0; y < x; y++){
        connections[x][y] = is_connected(x,y,state);
      }
    }
    var out = '';
    for (var x = 0; x < size; x++){
      for (var y = 0; y < size; y++){
        if(connections[x][y]) out += "+"; else out += '-';
      }
      out += '\n';
    }
    console.log(out);
    for(var node = 0; node < size; node++){
      console.log(degree(node, size, state), "degree of "+node);
    }
  }*/
}

/*

running for size:2
0 "ambiguous"
2 "unambiguous"
running for size:3
0 "ambiguous"
8 "unambiguous"
running for size:4
18 "ambiguous"
46 "unambiguous"
running for size:5
692 "ambiguous"
332 "unambiguous"
running for size:6
29894 "ambiguous"
2874 "unambiguous"
running for size:7
2068128 "ambiguous"
29024 "unambiguous"

*/