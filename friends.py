from sets import Set
import os.path
import urllib
import urllib2
import json
import re
import json

COOKIEFILE = 'cookies.lwp'
# the path and filename to save your cookies in

cj = None
ClientCookie = None
cookielib = None

# Let's see if cookielib is available
try:
  import cookielib
except ImportError:
  # If importing cookielib fails
  # let's try ClientCookie
  try:
    import ClientCookie
  except ImportError:
    # ClientCookie isn't available either
    urlopen = urllib2.urlopen
    Request = urllib2.Request
  else:
    # imported ClientCookie
    urlopen = ClientCookie.urlopen
    Request = ClientCookie.Request
    cj = ClientCookie.LWPCookieJar()

else:
  # importing cookielib worked
  urlopen = urllib2.urlopen
  Request = urllib2.Request
  cj = cookielib.LWPCookieJar()
  # This is a subclass of FileCookieJar
  # that has useful load and save methods

if cj is not None:
# we successfully imported
# one of the two cookie handling modules

  if os.path.isfile(COOKIEFILE):
    # if we have a cookie file already saved
    # then load the cookies into the Cookie Jar
    cj.load(COOKIEFILE)

  # Now we need to get our Cookie Jar
  # installed in the opener;
  # for fetching URLs
  if cookielib is not None:
    # if we use cookielib
    # then we get the HTTPCookieProcessor
    # and install the opener in urllib2
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
    urllib2.install_opener(opener)

  else:
    # if we use ClientCookie
    # then we get the HTTPCookieProcessor
    # and install the opener in ClientCookie
    opener = ClientCookie.build_opener(ClientCookie.HTTPCookieProcessor(cj))
    ClientCookie.install_opener(opener)


values = {
  "lsd": "",
  "email": "hsegfkuewrghksdf@gmail.com",
  "pass": "zxcvzxcvzxcv",
  "default_persistent": 0,
  "charset_test": "",
  "timezone": 480,
  "lgnrnd": "",
  "lgnjs": "",
  "locale": "en_US"
}


data = urllib.urlencode(values)
# req = urllib2.Request('https://www.facebook.com/login.php?login_attempt=1', data)
# response = urllib2.urlopen(req)
# print response.read()

theurl = 'https://www.facebook.com'
# an example url that sets a cookie,
# try different urls here and see the cookie collection you can make !

txdata = None
# if we were making a POST type request,
# we could encode a dictionary of values here,
# using urllib.urlencode(somedict)

txheaders = {'User-agent': 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'}
# fake a user agent, some websites (like google) don't like automated exploration

try:
  req = Request(theurl, txdata, txheaders)
  # create a request object

  handle = urlopen(req)
  # and open it to return a handle on the url

except IOError, e:
  print 'We failed to open "%s".' % theurl
  if hasattr(e, 'code'):
    print 'We failed with error code - %s.' % e.code
  elif hasattr(e, 'reason'):
    print "The error object has the following 'reason' attribute :"
    print e.reason
    print "This usually means the server doesn't exist,"
    print "is down, or we don't have an internet connection."
  sys.exit()

else:
  print 'Here are the headers of the page :'
  print handle.info()

  req = Request('https://www.facebook.com/login.php?login_attempt=1', data)
  response = urllib2.urlopen(req)
  f = open("login.html", "w")
  f.write(response.read())

  class Graph:
    links = {}  # map of user to set of users
    user_info = {}  # map of uid to user data
    positions = {}  # map of uid to position

    def connect(self, user1, users2):
      self.user_info[user1[0]] = user1
      if user1[0] in self.links.keys():
        self.links[user1[0]] |= users2
      else:
        self.links[user1[0]] = Set(users2)
      for user2 in users2:
        self.user_info[user2[0]] = user2
        if user2[0] in self.links.keys():
          self.links[user2[0]].add(user1)
        else:
          self.links[user2[0]] = Set([user1])

    def get_shared_connections(self, id1, id2, offset):
      u = "http://www.facebook.com/ajax/browser/dialog/mutual_friends/?uid=" + str(id1) + "&node=" + str(id2) + "&__a=1&start=" + str(offset)
      req = Request(u)
      retry = True
      while retry:
        retry = False
        try:
          response = urllib2.urlopen(req)
        except Exception, e:
          print e
          retry = True
      f = open("res.html", "w")
      # print response.info()
      res = response.read()
      f.write(res)
      # f = open("res.json", "w")
      # j = json.loads(res[len("for (;;);"):])
      # f.write(str(j))

      found = re.findall(r'user\.php\?id\=(\d+)\"\>([^\<]+)', res.decode('unicode-escape').encode('utf-8'))
      users = map(lambda f: (f[0], f[1], 1), found)  # 3rd parameter is public/private - 1 for public
      return users

    def find_all_public_friends(self, target, seed):
      self.connect(target, Set([seed]))
      scanned_friends = Set([])
      unscanned_friends = Set([seed])
      while len(unscanned_friends) > 0:
        friend = unscanned_friends.pop()
        ctr = 0
        while True:
          res = Set(self.get_shared_connections(target[0], friend[0], ctr))  # 100003225219808
          self.connect(target, res)
          self.connect(friend, res)
          unscanned_friends |= res
          if(len(res) < 30):
            break
          ctr += 30
        scanned_friends.add(friend)
        unscanned_friends -= scanned_friends
        print "found:", len(scanned_friends)
        print "queue size:", len(unscanned_friends)
        self.dump()
        # yield friend

    def dump(self):
      ff = open('friends_of_791992147.json', 'w', 0)
      fcsv = open('friends_of_791992147.csv', 'w', 0)
      position = 0
      nodes = []
      for uid, user in self.user_info.items():
        # map all users to positions for printing]
        self.positions[user[0]] = position
        position += 1
        nodes.append({'id': user[0], 'name': user[1], 'group': user[2]})
      edges = []
      for (id1, users2) in self.links.items():
        for user2 in users2:
          fcsv.write(str(id1) + ";" + str(user2[0]) + '\n')
          edges.append({'source': self.positions[id1], 'target': self.positions[user2[0]]})
      result = {"nodes": nodes, "links": edges}

      json.dump(result, ff)
      fcsv.close()
      ff.close()

  graph = Graph()

  print "scanning Zuck"
  graph.find_all_public_friends(('791992147', 'Eva Galperin', 1), ('501805642', 'Dan Tentler', 1))


  # handle.read() returns the page
  # handle.geturl() returns the true url of the page fetched
  # (in case urlopen has followed any redirects, which it sometimes does)

print
if cj is None:
  print "We don't have a cookie library available - sorry."
  print "I can't show you any cookies."
else:
  print 'These are the cookies we have received so far :'
  for index, cookie in enumerate(cj):
    print index, '  :  ', cookie
  cj.save(COOKIEFILE)                     # save the cookies again
