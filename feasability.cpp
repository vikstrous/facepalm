#include <iostream>
#include <unordered_map>
#include <string>
#include <math.h>

using namespace std;

bool is_connected(int x, int y, int state){
  //y is smaller or equal to x
  return !! (state & (1 << (x*(x-1)/2 + y)));
}

int degree(int node, int size, int state){
  int deg = 0;
  for(int x = 0; x < size; x++){
    if(node < x && is_connected(x,node,state))
      deg++;
    else if(node > x && is_connected(node,x,state))
      deg++;
  }
  return deg;
}

int main(int argc, char** argv){
  for ( int size = 2; size <= 8; size++){
    int max_width = log2(size) + 1;
    cout << "running for size: " << size << endl;

    unordered_map<long, int> collisions;

    int ambiguous = 0;
    int unambiguous = 0;
    for (int state = 0; state < pow(2, size*(size-1)/2); state++){
      long degrees = 0;
      for(int node = 0; node < size; node++){
        degrees += degree(node, size, state) << (max_width * node);
      }

      if(collisions.count(degrees)){
        if(collisions[degrees] == 1){
          collisions[degrees] = 2;
          ambiguous += 1;
          unambiguous -= 1;
        }
        ambiguous += 1;
      } else {
        collisions[degrees] = 1;
        unambiguous += 1;
      }
    }

    cout << ambiguous << " ambiguous" << endl;
    cout << unambiguous << " unambiguous" << endl;
  }
}
